# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import gnome with context %}

{# 
include:
  - {{ sls_config_clean }}
  #}

gnome-package-clean-pkg-removed:
  pkg.removed:
    - pkgs: {{ gnome.pkgs | yaml }}
    {#
    - require:
      - sls: {{ sls_config_clean }}
      #}
