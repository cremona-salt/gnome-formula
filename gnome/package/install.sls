# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import gnome with context %}

gnome-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ gnome.pkgs | yaml }}
