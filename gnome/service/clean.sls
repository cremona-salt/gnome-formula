# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import gnome with context %}

gnome-service-clean-service-dead:
  service.dead:
    - name: {{ gnome.service.name }}
    - enable: False
