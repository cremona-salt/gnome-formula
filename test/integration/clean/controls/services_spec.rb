# frozen_string_literal: true

# Overide by OS
service_name = 'gdm'

control 'gnome login service' do
  impact 0.5
  title 'should not be running and enabled'

  describe service(service_name) do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end
